package com.bankastudio.xando.Rooms;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Clipboard;
import com.bankastudio.xando.Room;
import com.bankastudio.xando.SockClient;
import com.bankastudio.xando.XandOMain;

public class ConnectScreen extends Room {
    private Tokiko tokiko;
    private Texture dialog_box;
    private BitmapFont font;

    private String dialog_text;
    private String dialog_input;
    private int blink_counter;
    private boolean is_input;
    private int state = 0;
    private int state_counter = 0;

    Game game_room;
    SockClient client;


    public void init() {
        super.init();
        tokiko = new Tokiko();
        tokiko.change(0,0,0);
        dialog_box = new Texture("dialog_box.png");
        dialog_text = "";
        dialog_input = "";

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font/UbuntuMono-R.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 30;
        parameter.color = Color.BLACK;
        parameter.characters = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"+
                "!\"#$%&\\'()*+,-./:;<=>?@[\\\\]^_`{|}~абвгдеёжзийклмнопрстуфхчшщъьыэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЧШЩЪЬЫЭЮЯ";
        font = generator.generateFont(parameter);
        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        if(game_room==null) {
            game_room = new Game();
            client = game_room.get_SocketClient();
        }
        state_counter=0;
    }

    @Override
    public void draw(SpriteBatch batch) {
        super.draw(batch);
        tokiko.draw(batch);

        float dialog_box_x = 800/2f-dialog_box.getWidth()/2f;
        batch.draw(dialog_box,dialog_box_x,10);
        if(!is_input)
            font.draw(batch, dialog_text,dialog_box_x+50,160);
        else {
            blink_counter++;
            if(blink_counter>10)
                blink_counter=-10;
            font.draw(batch, dialog_text + "\n>" + dialog_input + ((blink_counter>0) ? "_" : ""), dialog_box_x + 50, 160);
        }

    }

    public void set_state(int n_state){
        state = n_state;
    }

    @Override
    public void process() {
        super.process();

        switch (state){
            case 0:
                tokiko.change(0,3,0);
                dialog_text="Привет, это клиент для игры \"Крестики Нолики\"";
                state++;
                state_counter=0;
                is_input=false;
            break;
            case 1:
                state_counter++;
                if(state_counter>100) {
                    tokiko.change(0,0,0);
                    dialog_text+="\nМне нужен аддресс сервера для подключения.";
                    state++;
                    state_counter=0;
                }
            break;
            case 2:
                state_counter++;
                if(state_counter>100) {
                    tokiko.change(1,1,1);
                    dialog_text+="\nПожалуйста введи его сюда.";
                    is_input=true;
                    dialog_input = client.getServer_address();
                    state++;
                    state_counter=0;
                }
            break;
            case 3:
                state_counter++;
                if(state_counter>100) {
                    tokiko.change(1,0,0);
                    state++;
                    state_counter=0;
                }
                if(!is_input) state++;
            break;
            case 4:
                state_counter++;
                if(state_counter>100) {
                    tokiko.change(0,0,0);
                    state++;
                    state_counter=0;
                }
                if(!is_input) state++;
            break;
            case 5:
                if(dialog_input.length()>45){
                    dialog_input = dialog_input.substring(0,45);
                }
                if(!is_input){
                    client.setServer_address(dialog_input);
                    state++;
                    state_counter=0;
                    tokiko.change(0,3,1);
                    dialog_text="Отлично! Теперь номер порта.";
                    dialog_input=Integer.toString(client.getServer_port());
                    is_input=true;
                }
            break;
            case 6:
                if(state_counter>100) {
                    tokiko.change(0, 0, 0);
                }else
                    state_counter++;

                if(!is_input){
                    int port=0;
                    try {
                        port = Integer.parseInt(dialog_input);
                    }catch (NumberFormatException e){
                        dialog_text="Ошибка! Только цифры пожалуйста!";
                        tokiko.change(0, 3, 1);
                        is_input=true;
                        state_counter=0;
                        break;
                    }

                    client.setServer_port(port);
                    tokiko.change(0,4,1);
                    dialog_text="Адрес: "+ client.getServer_address()+":"+Integer.toString(client.getServer_port())+"\n"+
                    "Пытаюсь подключиться...";
                    state++;
                    state_counter=0;

                    client.connect();
                }
            break;
            case 7:
                if(state_counter>70) {
                    tokiko.change(0,4,0);
                }else state_counter++;

                if(client.is_error){
                    tokiko.change(0,0,0);
                    dialog_text="Адрес: "+ client.getServer_address()+":"+Integer.toString(client.getServer_port())+"\n"+
                    client.error_string+"\n"+
                    "1) Попробуем ещё раз?\n" +
                    "2) Или изменим параметры подключения?";
                    is_input=true;
                    dialog_input="";
                    state++;
                }
                if(client.is_connected){
                    tokiko.change(0,3,1);
                    state=9;
                    dialog_text="Подключение успешно!\n"+
                            "Пожалуйста введи свой логин:";
                    is_input=true;
                    dialog_input = client.getLogin();
                    state_counter=0;
                }

            break;
            case 8:
                if(!is_input) is_input=true;
                if(dialog_input.length()>1)
                    dialog_input = dialog_input.substring(dialog_input.length()-1);

                if(dialog_input.equals("1")){
                    is_input=false;
                    dialog_input = Integer.toString(client.getServer_port());
                    state=6;
                }
                if(dialog_input.equals("2")){
                    is_input=true;
                    dialog_input = client.getServer_address();
                    dialog_text = "Адрес сервера:";
                    state=5;
                    state_counter=0;
                }
            break;
            case 9:
                if(state_counter>50) {
                    tokiko.change(0,0,0);
                }else state_counter++;

                if(dialog_input.length()>20)
                    dialog_input = dialog_input.substring(0,20);

                if(!is_input) {
                    state++;
                    client.setLogin(dialog_input);
                    dialog_text = "Ожидаем второго игрока...";
                    state_counter=0;
                    tokiko.change(0,0,0);
                }

            break;
            case 10:
                if(state_counter>=0)
                    tokiko.y=-300;
                else
                    tokiko.y=-297;
                state_counter++;
                if(state_counter>50)
                    state_counter=-50;

                if(client.is_ready_to_start){
                    game_room.setConnect_screen(this);
                    XandOMain.change_room(game_room);
                    state++;
                }
            break;
            case 11:
                //win
                tokiko.change(0,3,1);
                dialog_text = "Поздравляю с победой!";
                if(state_counter>200) {
                    tokiko.change(0,0,0);
                    state = 7;
                    client.reset_flags();
                    client.is_error=true;
                    state_counter=0;
                }else state_counter++;
            break;
            case 12:
                //loose
                tokiko.change(0,0,1);
                dialog_text = "В следующий раз получится лучше !";
                if(state_counter>200) {
                    client.reset_flags();
                    client.is_error=true;
                    tokiko.change(0,0,0);
                    state = 7;
                    state_counter=0;
                }else state_counter++;
                break;
            case 13:
                //draw
                tokiko.change(0,3,0);
                dialog_text = "Ничья!";
                if(state_counter>200) {
                    tokiko.change(0,0,0);
                    state = 7;
                    client.reset_flags();
                    client.is_error=true;
                    state_counter=0;
                }else state_counter++;
                break;
            case 14:
                //disconected
                tokiko.change(0,0,0);
                state = 7;
                client.reset_flags();
                client.is_error=true;
                state_counter=0;
                break;

        }
        if(state>=9 && client.is_error){
            tokiko.change(0,0,0);
            state = 7;
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        tokiko.dispose();
        dialog_box.dispose();
        font.dispose();
    }

    private boolean _is_backspace = false;
    private boolean _is_control = false;
    @Override
    public boolean keyDown(int keycode) {
        if(is_input) {
            _is_backspace = false;
            if(keycode == Input.Keys.BACKSPACE){
                _is_backspace = true;
                if(dialog_input.length()>0)
                    dialog_input = dialog_input.substring(0,dialog_input.length()-1);
            }
            if(keycode == Input.Keys.ENTER){
                is_input=false;
            }
        }
        if(keycode == Input.Keys.CONTROL_LEFT || keycode == Input.Keys.CONTROL_RIGHT){
            _is_control=true;
        }
        if(_is_control && keycode == Input.Keys.V){
            Clipboard clip = Gdx.app.getClipboard();
            String t = clip.getContents();
            if(t.length()>0){
                dialog_input+=t;
            }
        }
        return super.keyDown(keycode);
    }

    @Override
    public boolean keyUp(int keycode){

        if(keycode == Input.Keys.CONTROL_LEFT || keycode == Input.Keys.CONTROL_RIGHT){
            _is_control=false;
        }

        return super.keyUp(keycode);
    }

    @Override
    public boolean keyTyped(char character) {
        if(is_input){

            if(!_is_backspace && !_is_control)
                dialog_input+=character;
        }

        return super.keyTyped(character);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        /*if(is_input){
            Clipboard clip = Gdx.app.getClipboard();
            String t = clip.getContents();
            if(t.length()>0){
                dialog_input+=t;
            }
        }*/
        return super.touchDown(screenX, screenY, pointer, button);
    }


    private class Tokiko{
        Texture[] body;
        Texture[] eyes;
        Texture[] mouth;

        int body_id = 0;
        int eyes_id = 0;
        int mouth_id = 0;

        int blink_counter=-1;
        int blink_counter_limit=100;
        int before_blink_id=-1;

        float x,y;
        Tokiko(){
            x=0;
            y=-300;
            body = new Texture[2];
            for(int i=0;i<body.length;i++) {
                body[i] = new Texture("tokiko/tokiko_body_" + Integer.toString(i) + ".png");
                body[i].setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }

            eyes = new Texture[5];
            for(int i=0;i<eyes.length-1;i++) {
                eyes[i] = new Texture("tokiko/tokiko_eyes_" + Integer.toString(i) + ".png");
                eyes[i].setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
            eyes[4]=eyes[2];
            mouth = new Texture[2];
            for(int i=0;i<mouth.length;i++) {
                mouth[i] = new Texture("tokiko/tokiko_mouth_" + Integer.toString(i) + ".png");
                mouth[i].setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }

        }

        void change(int _body, int _eyes, int _mouth){
            if(_body<body.length)
                body_id = _body;
            if(_eyes<eyes.length)
                eyes_id = _eyes;
            if(_mouth<mouth.length)
                mouth_id = _mouth;

            //blink_counter=;
            //blink_counter_limit=100;
            before_blink_id=-1;
        }

        void draw(SpriteBatch batch){
            x=800f/2-body[0].getWidth()/2f;

            if( (eyes_id==0) || (eyes_id == 1) || (eyes_id == 2) ){
                if(blink_counter<0){
                    if(before_blink_id<0) {
                        before_blink_id = eyes_id;
                        eyes_id = 2;
                    }
                }else
                if(blink_counter==0){
                    if(before_blink_id>=0) {
                        eyes_id = before_blink_id;
                        before_blink_id = -1;
                    }
                }
            }
            blink_counter++;
            if(blink_counter>blink_counter_limit){
                blink_counter=-15;
                blink_counter_limit = (int) Math.floor(Math.random()*600+100);
            }

            batch.draw(body[body_id],x,y);
            batch.draw(eyes[eyes_id],x,y);
            batch.draw(mouth[mouth_id],x,y);
        }

        void dispose(){
            for(int i=0;i<body.length;i++)
                body[i].dispose();
            for(int i=0;i<eyes.length;i++)
                eyes[i].dispose();
            for(int i=0;i<mouth.length;i++)
                mouth[i].dispose();
        }
    }
}

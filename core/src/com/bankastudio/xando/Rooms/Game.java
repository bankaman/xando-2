package com.bankastudio.xando.Rooms;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.bankastudio.xando.Button;
import com.bankastudio.xando.Room;
import com.bankastudio.xando.SockClient;
import com.bankastudio.xando.XandOMain;
import com.bankastudio.xando.logic.XandOLogic;


public class Game extends Room {

    private Texture field_element;
    private Texture cross;
    private Texture maru;
    private Texture draw;
    private Texture win;
    private Texture loose;
    private Texture remote_cursor;

    private Button btn_back;

    BitmapFont font;


    int el_size;
    int field_width;
    int field_height;
    int field_x;
    int field_y;

    GlyphLayout layout;

    XandOLogic XandO;
    SockClient client;
    ConnectScreen connect_screen;

    public Game(){
        XandO = new XandOLogic();
        client = new SockClient();
        client.setLogic(XandO);
        connect_screen = null;
    }

    public SockClient get_SocketClient(){
        return client;
    }

    public void setConnect_screen(ConnectScreen connect_screen) {
        this.connect_screen = connect_screen;
    }

    @Override
    public void init() {
        super.init();

        field_element = new Texture("field_element.png");
        cross = new Texture("cross.png");
        maru = new Texture("maru.png");
        draw = new Texture("text/draw.png");
        win = new Texture("text/win.png");
        loose = new Texture("text/loose.png");
        remote_cursor = new Texture("cursor.png");

        btn_back = new Button("button_back.png");
        btn_back.x = 20;
        btn_back.y = 20;

        cross.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        maru.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        el_size = 760/XandO.getField_width();
        if(el_size*XandO.getField_height()>500)
            el_size = 480/XandO.getField_width();

        field_width=el_size*XandO.getField_width();
        field_height=el_size*XandO.getField_height();
        field_x = 800/2-field_width/2;
        field_y = 500/2-field_height/2+100;

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font/UbuntuMono-R.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 30;
        parameter.color = Color.BLACK;
        parameter.characters = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"+
                "!\"#$%&\\'()*+,-./:;<=>?@[\\\\]^_`{|}~абвгдеёжзийклмнопрстуфхчшщъьыэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЧШЩЪЬЫЭЮЯ";
        font = generator.generateFont(parameter);
        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        layout = new GlyphLayout();
    }

    @Override
    public void draw(SpriteBatch batch) {
        super.draw(batch);

        //drawing field
        int x,y,item;
        for(int i=0;i<XandO.getField_height();i++){
            for(int j=0;j<XandO.getField_width();j++){
                x = field_x +j*el_size;
                y = field_y+i*el_size;
                item = XandO.get_safe_field_item(j,i);
                batch.draw(field_element,x,y,el_size,el_size);
                if(item==1)
                    batch.draw(cross,x,y,el_size,el_size);
                else if(item==2)
                    batch.draw(maru,x,y,el_size,el_size);
            }
        }

        if(XandO.getWinner()>=0){
            if(XandO.getWinner()==0)
                batch.draw(draw,800/2-draw.getWidth()/2,30);
            else if(XandO.getWinner()==client.get_player_id()+1)
                batch.draw(win,800/2-win.getWidth()/2,30);
            else
                batch.draw(loose,800/2-loose.getWidth()/2,30);
        }else {
            if(client.is_connected) {
                if (XandO.get_current_player() == client.get_player_id() + 1) {
                    layout.setText(font, "Ваш ход");
                    font.draw(batch, "Ваш ход", 800 / 2 - layout.width / 2, 100);
                } else {
                    layout.setText(font, "Сейчас ходит " + client.get_remote_login());
                    font.draw(batch, "Сейчас ходит " + client.get_remote_login(), 800 / 2 - layout.width / 2, 100);
                }
            }
        }

        if(!client.is_connected){
            if(XandO.getWinner()<0){
                layout.setText(font, "Оппонент отключился");
                font.draw(batch, "Оппонент отключился", 800 / 2 - layout.width / 2, 100);
            }

            btn_back.draw(batch);
        }
        //btn_back.process(this);


        batch.draw(remote_cursor,client.remote_mouse_x, client.remote_mouse_y-remote_cursor.getWidth());
    }

    @Override
    public void process() {
        super.process();

        if(client.is_error){
            connect_screen.set_state(7);
            XandOMain.change_room(connect_screen);
        }

    }


    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if(!client.is_connected) {
            if (mouse_hit_button(btn_back)) {
                if (XandO.getWinner() == client.get_player_id() + 1) {
                    connect_screen.set_state(11);

                } else if (XandO.getWinner() == 0) {
                    connect_screen.set_state(13);
                } else if (XandO.getWinner() < 0) {
                    connect_screen.set_state(14);
                } else {
                    connect_screen.set_state(12);
                }
                XandOMain.change_room(connect_screen);
            }
        }

        if(XandO.getWinner()<0 && client.is_connected) {
            //checking is user clicked on field
            if (mouse_x > field_x && mouse_x < field_x + field_width && mouse_y > field_y && mouse_y < field_y + field_height) {
                int col = (mouse_x - field_x) / el_size;
                int row = (mouse_y - field_y) / el_size;

                client.make_turn(row,col);
                /*
                if (XandO.make_turn(row, col, 1)) {
                    if(XandO.getWinner()<0)
                        XandO.AI_turn(2);
                }*/
            }
        }

        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        boolean t = super.mouseMoved(screenX, screenY);

        client.set_mouse_position(mouse_x,mouse_y);

        return t;
    }


    @Override
    public void dispose() {
        super.dispose();

        field_element.dispose();
        cross.dispose();
        maru.dispose();
        draw.dispose();
        win.dispose();
        loose.dispose();
        remote_cursor.dispose();
    }

}

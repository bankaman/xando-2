package com.bankastudio.xando;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Button {
    public int x;
    public int y;
    public int w;
    public int h;

    public float alpha;

    private Texture texture;
    private Color c;

    public Button(String fname){
        texture = new Texture(fname);
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        x=0;
        y=0;
        w=texture.getWidth();
        h=texture.getHeight();
        alpha=1;
    }

    public void draw(SpriteBatch batch){
        c = batch.getColor();
        batch.setColor(1,1,1,alpha*c.a);
        batch.draw(texture,x,y,w,h);
        batch.setColor(c);
    }

    public void process(Room r){
        if(r.mouse_hit_button(this) && alpha<1)
            alpha+=0.05;
        else if(alpha>0.7)
            alpha-=0.05;
    }

    public void dispose(){
        texture.dispose();
    }
}

package com.bankastudio.xando;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.bankastudio.xando.Rooms.ConnectScreen;

import static com.badlogic.gdx.Application.LOG_INFO;

public class XandOMain extends ApplicationAdapter {
	private SpriteBatch batch;

	private static Room current_room=null;
	private static Room fading_room=null;

	private ExtendViewport viewport;
	static OrthographicCamera camera;

	public static BitmapFont font;

	private static boolean do_fade=false;
	private static float zoom_old;
	private static float zoom_new;
	private static float old_alpha;
	private static float new_alpha;
	private static Color c;

	public static void change_room(Room r){
		if(current_room!=null) {
			if(fading_room!=null)
				fading_room.dispose();
			fading_room=current_room;
			do_fade=true;
			zoom_old=1f;
			zoom_new=10.0f;
			old_alpha=1f;
			new_alpha=0f;
		}
		current_room = r;
		Gdx.input.setInputProcessor(current_room);
		r.init();
	}

	@Override
	public void create() {
		super.create();
		Gdx.app.setLogLevel(LOG_INFO);
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		viewport = new ExtendViewport(800,600,camera);
		viewport.apply();
		camera.position.set(800/2,600/2,0);

		Gdx.graphics.setTitle("XandO Client");

		change_room(new ConnectScreen());
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		viewport.update(width,height);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0.7f, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();


        if(do_fade) {
            camera.zoom = zoom_new;
            c=batch.getColor();
            batch.setColor(c.r,c.g,c.b,new_alpha);
        }

		camera.update();
		batch.setProjectionMatrix(camera.combined);



		if(current_room!=null) {
			current_room.draw(batch);
			current_room.process();
		}


		if(do_fade){
			camera.zoom=zoom_old;
			camera.update();
			batch.setProjectionMatrix(camera.combined);
            c=batch.getColor();
			batch.setColor(c.r,c.g,c.b,old_alpha);

			fading_room.draw(batch);

			batch.setColor(c.r,c.g,c.b,1);
			zoom_old+=(0.1f-zoom_old)/10;
			zoom_new+=(1f-zoom_new)/10;
			old_alpha+=(0-old_alpha)/10;
			new_alpha+=(1-new_alpha)/10;
			if(Math.abs(1f-zoom_new) < 0.001) {
				do_fade = false;
				fading_room.dispose();
				fading_room=null;
				camera.zoom=1f;
			}
		}

		batch.end();
	}
	
	@Override
	public void dispose () {
		if(current_room!=null)
			current_room.dispose();
		if(fading_room!=null)
			fading_room.dispose();
		batch.dispose();
	}
}

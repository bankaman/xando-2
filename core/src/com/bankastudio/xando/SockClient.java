package com.bankastudio.xando;

import com.bankastudio.xando.logic.NetProto;
import com.bankastudio.xando.logic.XandOLogic;

import java.io.IOException;
import java.net.Socket;
import java.util.Vector;

public class SockClient{
    String server_address = "127.0.0.1";
    int server_port = 3000;
    String login = "player";
    String remote_login;
    Runnable thread;
    XandOLogic logic;

    int player_id;

    public boolean is_error = false;
    public String error_string;

    public boolean is_connected = false;
    public boolean is_waiting_for_player = false;
    public boolean  is_ready_to_start = false;

    int mouse_x,mouse_y;
    int last_mouse_x,last_mouse_y;

    public int remote_mouse_x, remote_mouse_y;

    public String getServer_address() {
        return server_address;
    }

    public int getServer_port() {
        return server_port;
    }

    public void setServer_address(String server_address) {
        this.server_address = server_address;
    }

    public void setServer_port(int server_port) {
        this.server_port = server_port;
    }

    public void setLogic(XandOLogic _logic){
        logic = _logic;
    }


    public String getLogin(){return login;}
    public void setLogin(String _login){
        login=_login;
        try {
            thread.proto.send_login(login);
        } catch (IOException e) {
            e.printStackTrace();
            is_error=true;
        }
    }

    public void reset_flags(){
        is_error = false;
        is_connected = false;
        is_waiting_for_player = false;
        is_ready_to_start = false;
        remote_login = null;
        player_id=-1;
        error_string="";
    }

    public boolean connect(){
        reset_flags();
        logic.clear_field();
        if( (thread != null) && (thread.isAlive())) {
            System.out.println("Thread still alive");
            error_string = "Thread still alive";
            is_error = true;
            return false;
        }

        thread = new Runnable();
        thread.start();

        return true;
    }

    public void set_mouse_position(int x, int y){
        mouse_x = x;
        mouse_y = y;
    }

    public String get_remote_login(){
        if(remote_login==null)
            return "";
        return remote_login;
    }

    public int get_player_id(){
        return player_id;
    }

    public void make_turn(int row, int col) {
        try {
            thread.proto.send_make_turn(row, col);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class Runnable extends Thread {

        Socket socket;
        NetProto proto;
        boolean do_run;

        @Override
        public void run() {
            super.run();
            do_run=true;
            try {
                socket = new Socket(server_address, server_port);
                socket.setSoTimeout(5000);
                proto = new NetProto(socket);
            } catch (IOException e) {
                e.printStackTrace();
                is_error = true;
                error_string = e.getMessage();
                return;
            }
            NetProto.Packet packet;
            long last_timestamp = -1;
            while (do_run){
                try {
                    if(System.currentTimeMillis()-last_timestamp>10000){
                        proto.send_ping();
                        last_timestamp = System.currentTimeMillis();
                    }

                    packet = proto.read_packet();
                    if(packet==null) {
                        sleep(30);
                    }else {
                        switch (packet.packet_id) {
                            case NetProto.PACKET_ID_PING:
                                proto.send_pong();
                                break;
                            case NetProto.PACKET_ID_PONG:
                                last_timestamp = System.currentTimeMillis();
                                break;
                            case NetProto.PACKET_ID_LOGIN_REQUEST:
                                is_connected = true;
                                break;
                            case NetProto.PACKET_ID_WAIT_FOR_PLAYER:
                                is_waiting_for_player = true;
                                break;
                            case NetProto.PACKET_ID_DISCONNECT:
                                do_run = false;
                                System.out.println("Disconnect packet!");
                                is_connected=false;
                                break;
                            case NetProto.PACKET_ID_START_GAME:
                                is_ready_to_start = true;
                                player_id = proto.decode_packed_int(packet.data);
                                remote_login = proto.decode_string(packet.data);
                                break;
                            case NetProto.PACKET_ID_MOUSE_POSITION:
                                remote_mouse_x = proto.decode_packed_int(packet.data);
                                remote_mouse_y = proto.decode_packed_int(packet.data);
                                break;
                            case NetProto.PACKET_ID_TURN_INFO: {
                                int player_id = proto.decode_packed_int(packet.data);
                                int row = proto.decode_packed_int(packet.data);
                                int col = proto.decode_packed_int(packet.data);
                                System.out.println("TURN by " + player_id + " row:" + row + " col:" + col);
                                logic.make_turn(row, col, player_id);
                            }
                            break;

                            default:
                                error_string = "Unexpected packet_id: " + packet.packet_id;
                                is_error = true;
                                do_run = false;
                        }
                    }
                    if( is_ready_to_start && is_connected) {
                        if((mouse_x != last_mouse_x || mouse_y != last_mouse_y)){
                            proto.send_mouse_position(mouse_x,mouse_y);
                            last_mouse_x = mouse_x;
                            last_mouse_y = mouse_y;
                        }
                    }

                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                    is_error = true;
                    error_string = e.getMessage();
                    do_run=false;
                }
            }
            is_connected=false;
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Socket thread ended");
        }
    }
}

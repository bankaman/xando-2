package com.bankastudio.xando;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Room implements InputProcessor {

    protected int mouse_x;
    protected int mouse_y;

    public void init(){

    }
    public void draw(SpriteBatch batch){

    }

    public void process(){

    }

    public void dispose(){

    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {

        mouse_x= (int) (((screenX* XandOMain.camera.viewportWidth)/ Gdx.graphics.getWidth())-XandOMain.camera.viewportWidth/2+800/2);
        mouse_y= (int) (XandOMain.camera.viewportHeight-((screenY* XandOMain.camera.viewportHeight)/ Gdx.graphics.getHeight())-XandOMain.camera.viewportHeight/2+600/2);
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public boolean mouse_hit_button(Button b){
        return (
                (mouse_x>b.x && mouse_x<b.x+b.w) &&
                        ((mouse_y)>b.y && mouse_y<b.y+b.h)
        );
    }
}

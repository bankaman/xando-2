package com.bankastudio.xando.logic;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class NetProto {
    public final static byte PACKET_ID_PING = 0;
    public final static byte PACKET_ID_PONG = 1;
    public final static byte PACKET_ID_LOGIN_REQUEST = 2;
    public final static byte PACKET_ID_LOGIN = 3;
    public final static byte PACKET_ID_START_GAME = 4;
    public final static byte PACKET_ID_DISCONNECT = 5;
    public final static byte PACKET_ID_WAIT_FOR_PLAYER = 6;
    public final static byte PACKET_ID_MOUSE_POSITION = 7;
    public final static byte PACKET_ID_MAKE_TURN = 8;
    public final static byte PACKET_ID_TURN_INFO = 9;


    Socket socket;
    OutputStream out;
    InputStream  in;

    public NetProto(Socket _socket) throws IOException {
        socket = _socket;
        out = socket.getOutputStream();
        in = socket.getInputStream();

    }

    public void send_ping() throws IOException {
        ByteBuffer packet = make_packet(PACKET_ID_PING);
        out.write(packet.array());
        out.flush();
    }
    public void send_pong() throws IOException {
        ByteBuffer packet = make_packet(PACKET_ID_PONG);
        out.write(packet.array());
        out.flush();
    }
    public void send_login_request() throws IOException {
        ByteBuffer packet = make_packet(PACKET_ID_LOGIN_REQUEST);
        out.write(packet.array());
        out.flush();
    }

    public void send_login(String login) throws IOException {
        ByteBuffer data = code_string(login);
        ByteBuffer packet = make_packet(PACKET_ID_LOGIN, data);
        out.write(packet.array());
        out.flush();
    }
    public void send_start_game(int player_id, String opponent_login) throws IOException {
        ByteBuffer id = code_packed_int(player_id);
        ByteBuffer log = code_string(opponent_login);
        ByteBuffer data = ByteBuffer.allocate(id.limit()+log.limit());
        data.put(id);
        data.put(log);
        ByteBuffer packet = make_packet(PACKET_ID_START_GAME,data);
        out.write(packet.array());
        out.flush();
    }
    public void send_disconnect() throws IOException {
        ByteBuffer packet = make_packet(PACKET_ID_DISCONNECT);
        out.write(packet.array());
        out.flush();
    }
    public void send_wait_for_player() throws IOException {
        ByteBuffer packet = make_packet(PACKET_ID_WAIT_FOR_PLAYER);
        out.write(packet.array());
        out.flush();
    }
    public void send_mouse_position(int mouse_x, int mouse_y) throws IOException {
        ByteBuffer x = code_packed_int(mouse_x);
        ByteBuffer y = code_packed_int(mouse_y);
        ByteBuffer data = ByteBuffer.allocate(x.limit()+y.limit());
        data.put(x);
        data.put(y);
        ByteBuffer packet = make_packet(PACKET_ID_MOUSE_POSITION,data);
        out.write(packet.array());
        out.flush();
    }
    public void send_make_turn(int _row, int _col) throws IOException {
        ByteBuffer row = code_packed_int(_row);
        ByteBuffer col = code_packed_int(_col);
        ByteBuffer data = ByteBuffer.allocate(row.limit()+col.limit());
        data.put(row);
        data.put(col);
        ByteBuffer packet = make_packet(PACKET_ID_MAKE_TURN,data);
        out.write(packet.array());
        out.flush();
    }

    public void send_turn_info(int _player, int _row, int _col) throws IOException {
        ByteBuffer player = code_packed_int(_player);
        ByteBuffer row = code_packed_int(_row);
        ByteBuffer col = code_packed_int(_col);
        ByteBuffer data = ByteBuffer.allocate(row.limit()+col.limit()+player.limit());
        data.put(player);
        data.put(row);
        data.put(col);
        ByteBuffer packet = make_packet(PACKET_ID_TURN_INFO,data);
        out.write(packet.array());
        out.flush();
    }

    public Packet read_packet() throws IOException {
        if(in.available()==0)
            return null;
        Packet p;
        p = new Packet();
        p.packet_id = (byte) in.read();
        //System.out.println("Packet_id: " + p.packet_id+ "; avalible:"+in.available());
        int len = read_packed_int();
        if(len<0)
            return p;
        //System.out.println("len: " + len);
        ByteBuffer buffer;
        buffer = ByteBuffer.allocate(len);
        int t = in.read(buffer.array(), 0, len);
        p.data = buffer;
        p.data.position(0);
        return p;
    }

    private ByteBuffer make_packet(byte packet_id){
        return  make_packet(packet_id,ByteBuffer.allocate(0));
    }

    private ByteBuffer make_packet(byte packet_id, ByteBuffer data){
        ByteBuffer packet;
        ByteBuffer length;
        int len = data.limit();
        length = code_packed_int(len);
        length.position(0);
        packet = ByteBuffer.allocate(data.limit()+length.limit()+1);
        packet.put(packet_id);
        packet.put(length);
        data.position(0);
        packet.put(data);
        packet.position(0);
        return packet;
    }

    ByteBuffer code_string(String val){
        ByteBuffer buf;
        ByteBuffer str = ByteBuffer.wrap(val.getBytes(StandardCharsets.UTF_8));
        ByteBuffer length = code_packed_int(str.limit());
        buf = ByteBuffer.allocate(str.limit()+length.limit());
        buf.put(length);
        buf.put(str);
        buf.position(0);
        return buf;
    }

    public String decode_string(ByteBuffer buf){
        String out;
        int len = decode_packed_int(buf);
        ByteBuffer temp = ByteBuffer.allocate(len);
        buf.get(temp.array(),0,len);
        out = new String(temp.array(),StandardCharsets.UTF_8);
        return out;
    }

    public ByteBuffer code_packed_int(int _val){
        long val = _val&0xFFFFFFFFL;
        ArrayList<Byte> temp = new ArrayList<Byte>();
        if(val==0){
            temp.add((byte) val);
        }
        while (val!=0) {
            byte t = (byte)(val&0b01111111);
            val = val >> 7;
            if(val>0){
                t |= 0b10000000;
            }
            temp.add(t);
        }
        return convert_to_bytebuffer(temp);
    }

    public int decode_packed_int(ByteBuffer buff){
        int val;
        val = buff.get();
        if((val&0x80)==0)
            return val;

        val = (val&0x7f) | buff.get() << 7;
        if((val&0x4000)==0)
            return val;

        val = (val&0x3fff) | buff.get() << 14;
        if((val&0x200000)==0)
            return val;

        val = (val&0x1fffff) | buff.get() << 21;
        if((val&0x10000000)==0)
            return val;
        val = (val&0x0fffffff) | buff.get() << 28;

        return val;
    }
    int read_packed_int() throws IOException {
        int val;
        val = in.read();
        if((val&0x80)==0)
            return val;

        val = (val&0x7f) | in.read() << 7;
        if((val&0x4000)==0)
            return val;

        val = (val&0x3fff) | in.read() << 14;
        if((val&0x200000)==0)
            return val;

        val = (val&0x1fffff) | in.read() << 21;
        if((val&0x10000000)==0)
            return val;
        val = (val&0x0fffffff) | in.read() << 28;

        return val;
    }

    private ByteBuffer convert_to_bytebuffer(ArrayList<Byte> in){
        ByteBuffer out = ByteBuffer.allocate(in.size());
        for(int i=0;i<in.size();i++){
            out.put(in.get(i));
        }
        out.position(0);
        return out;
    }




    public class Packet{
        public byte packet_id;
        public ByteBuffer data;
    }

}

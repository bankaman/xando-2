package com.bankastudio.xando.logic;

import java.net.Socket;

public class XandOLogic {
    private int field_width;
    private int field_height;
    private int winner;
    private int current_turn_player;

    private byte field[][];

    public XandOLogic(){
        field_width  = 10;
        field_height = 10;
        winner = -1;
        field = new byte[field_width][field_height];
        current_turn_player = 1;
    }

    public byte get_safe_field_item(int col,int row){
        if(row < 0 || row >= field_height) return -1;
        if(col < 0 || col >= field_width)  return -1;
        return field[col][row];
    }

    public boolean make_turn(int row, int col, int player){
        if(row<0 || row>= field_height) return false;
        if(col<0 || col>= field_width) return false;
        if(player!=current_turn_player)
            return false;

        if(field[col][row] == 0)
            field[col][row] = (byte)player;
        else
            return false;

        byte t;
        boolean test;

        loop:
        for(int i=0;i<field_height;i++)
            for(int j=0;j<field_width;j++){
                t=get_safe_field_item(j,i);
                if(t==0) continue;
                test=true;
                for(int k=0;k<5;k++)
                    if(get_safe_field_item(j+k,i)!=t) {
                        test=false;
                        break;
                    }

                if(test) {
                    winner = t;
                    break loop;
                }
                test=true;
                for(int k=0;k<5;k++)
                    if(get_safe_field_item(j+k,i+k)!=t) {
                        test=false;
                        break;
                    }
                if(test) {
                    winner = t;
                    break loop;
                }
                test=true;
                for(int k=0;k<5;k++)
                    if(get_safe_field_item(j+k,i-k)!=t) {
                        test=false;
                        break;
                    }
                if(test) {
                    winner = t;
                    break loop;
                }
                test=true;
                for(int k=0;k<5;k++)
                    if(get_safe_field_item(j,i+k)!=t) {
                        test=false;
                        break;
                    }

                if(test) {
                    winner = t;
                    break loop;
                }

            }

        if(winner<0 && !is_place_on_field())
            winner=0;

        if(player==1)
            current_turn_player=2;
        if(player==2)
            current_turn_player=1;
        return true;
    }

    private float _calc_score(int empty,int my,int alien){
        float sc;
        sc=0;

        if(empty==1 && my==4) sc=5;
        if(empty==1 && alien==4) sc=5;
        if(empty==2 && alien==3) sc=4;
        if(empty==2 && my==3) sc=4;
        if(empty==3 && my==2) sc=3;
        if(empty==3 && alien==2) sc=3;
        if(empty==4 && alien==1) sc=2;
        if(empty==4 && my==1) sc=2;

        return sc;
    }

    public void AI_turn(int player){

        //creating score array
        float score[][] = new float[field_width][field_height];
        for (int i=0;i<field_height;i++)
            for (int j=0;j<field_width;j++)
                score[j][i] = 0;

        //calculate score
        int empty_count;
        int my_count;
        int alien_count;
        boolean skip=false;
        byte t;
        float sc;
        for(int i=0;i<=field_height;i++){
            for (int j=0;j<=field_width;j++){


                empty_count=0;
                my_count=0;
                alien_count=0;
                skip=false;

                for(int k=0;k<5;k++) {
                    t=get_safe_field_item(j+k,i);
                    if(t==0) empty_count++;
                    else if (t==player) my_count++;
                    else if (t<0) {skip=true; break;}
                    else alien_count++;

                }
                if(!skip) {
                    sc = _calc_score(empty_count,my_count,alien_count);

                    for (int k = 0; k < 5; k++) {
                        if (get_safe_field_item(j + k, i) == 0) {
                            if(sc>score[j + k][i])
                                score[j + k][i] = sc;
                        }
                    }
                }

                empty_count=0;
                my_count=0;
                alien_count=0;
                skip=false;
                for(int k=0;k<5;k++) {
                    t=get_safe_field_item(j+k,i+k);
                    if(t==0) empty_count++;
                    else if (t==player) my_count++;
                    else if (t<0) {skip=true; break;}
                    else alien_count++;

                }
                if(!skip) {
                    sc = _calc_score(empty_count,my_count,alien_count);
                    for (int k = 0; k < 5; k++) {
                        if (get_safe_field_item(j + k, i + k) == 0)
                            if(sc>score[j + k][i + k])
                                score[j + k][i + k] =sc;
                    }
                }

                empty_count=0;
                my_count=0;
                alien_count=0;
                skip=false;

                for(int k=0;k<5;k++) {
                    t=get_safe_field_item(j+k,i-k);
                    if(t==0) empty_count++;
                    else if (t==player) my_count++;
                    else if (t<0) {skip=true; break;}
                    else alien_count++;

                }
                if(!skip) {
                    sc = _calc_score(empty_count,my_count,alien_count);
                    for (int k = 0; k < 5; k++) {
                        if (get_safe_field_item(j + k, i - k) == 0)
                            if(sc>score[j + k][i - k])
                                score[j + k][i - k] = sc;
                    }
                }

                empty_count=0;
                my_count=0;
                alien_count=0;
                skip=false;

                for(int k=0;k<5;k++) {
                    t=get_safe_field_item(j,i+k);
                    if(t==0) empty_count++;
                    else if (t==player) my_count++;
                    else if (t<0) {skip=true; break;}
                    else alien_count++;
                }
                if(!skip) {
                    sc = _calc_score(empty_count,my_count,alien_count);
                    for (int k = 0; k < 5; k++) {
                        if (get_safe_field_item(j, i + k) == 0)
                            if(sc>score[j][i + k])
                                score[j][i + k] = sc;
                    }
                }

            }
        }

        //debug
        /*System.out.println();
        for(int i=0;i<field_height;i++) {
            for (int j = 0; j < field_width; j++) {
                System.out.print(score[j][i] + " | ");
            }
            System.out.println();
        }*/

        //finding maximum score
        int col=0,row=0;
        float max=-999;

        for (int i=0;i<field_height;i++)
            for(int j=0;j<field_width;j++){
                if(max<score[j][i]) {
                    max = score[j][i];
                    col = j;
                    row = i;
                }
            }

        while(!make_turn(row,col,player)){
            col++;
            if(col>=field_width){
                col=0;
                row++;
            }
            if(!is_place_on_field()) break;
        }
    }

    public boolean is_place_on_field(){
        for(int i=0;i<field_height;i++)
            for(int j=0;j<field_width;j++)
                if(get_safe_field_item(j,i)==0) return true;
        return false;
    }

    public void set_field_size(int w, int h){
        field_width  = w;
        field_height = h;
        field = new byte[field_width][field_height];
    }
    public void clear_field(){
        winner=-1;
        current_turn_player = 1;
        for (int j=0;j<field_height;j++)
            for (int i=0;i<field_width;i++)
                field[i][j]=0;
    }


    public byte[][] get_field(){
        return field;
    }
    public int getWinner(){return winner;}
    public int getField_width(){ return field_width; }
    public int getField_height(){ return field_height; }
    public int get_current_player(){
        return  current_turn_player;
    }

}

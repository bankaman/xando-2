package com.bankastudio.xando.server;

import com.bankastudio.xando.logic.NetProto;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

public class Client extends Thread{
    Socket socket;
    NetProto proto;
    GameRoom room;

    boolean do_running;
    String player_login=null;
    int id = -1;

    public Client(Socket _socket){
        socket = _socket;
        try {
            socket.setSoTimeout(5000);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        try {
            proto = new NetProto(socket);
        } catch (IOException e) {
            e.printStackTrace();
        }
        player_login=null;
        room=null;
    }

    @Override
    public void run() {
        super.run();
        ServerLauncher.log("New client!");

        do_running = true;
        NetProto.Packet packet;

        try {
            proto.send_ping();
        } catch (IOException e) {
            e.printStackTrace();
            do_running=false;
        }

        long last_timestamp = -1;

        while (do_running){

            try {

                if(System.currentTimeMillis()-last_timestamp>1000){
                    proto.send_ping();
                    last_timestamp = System.currentTimeMillis();
                }
                if(player_login==null){
                    player_login="";
                    proto.send_login_request();
                }

                packet = proto.read_packet();
                if(packet==null) {
                    sleep(30);
                    continue;
                }

                switch (packet.packet_id){
                    case NetProto.PACKET_ID_PING:
                        proto.send_pong();
                    break;
                    case NetProto.PACKET_ID_PONG:
                    break;
                    case NetProto.PACKET_ID_LOGIN:
                        player_login = proto.decode_string(packet.data);
                        room = Server.get_room();
                        id = room.register_player(this);
                        ServerLauncher.log("player_login: "+player_login+"; id: "+id+"; room_id: "+room.id);
                        if(room.is_full()){
                            room.send_start_game();
                        }else{
                            proto.send_wait_for_player();
                        }
                    break;
                    case NetProto.PACKET_ID_MOUSE_POSITION:
                        room.send_mouse_position(id,proto.decode_packed_int(packet.data),proto.decode_packed_int(packet.data));
                    break;
                    case  NetProto.PACKET_ID_MAKE_TURN:
                        room.make_turn(id,proto.decode_packed_int(packet.data),proto.decode_packed_int(packet.data));
                    break;

                }

            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                do_running = false;
            }
        }

        if(room!=null){
            room.disconnect(id);
        }
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ServerLauncher.log("Client thread done room: "+room.id+"; player:"+id);
    }

    public void close(){
        try {
            proto.send_disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        do_running = false;
    }


}

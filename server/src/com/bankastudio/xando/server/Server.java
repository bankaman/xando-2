package com.bankastudio.xando.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Vector;

class Server {
    private int portnum;
    private static boolean do_run=true;

    private static Vector<GameRoom> rooms=null;

    Server(int _portnum){
        portnum = _portnum;
        if(rooms == null)
            rooms = new Vector<GameRoom>();
    }

    boolean run(){
        ServerSocket socket;
        try {
            socket = new ServerSocket(portnum);
        } catch (IOException e) {
            ServerLauncher.log("Error opening port #"+portnum);
            ServerLauncher.log(e.getMessage());
            return false;
        }
        do_run=true;
        while (do_run){
            try {
                (new Client(socket.accept())).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        

        return true;
    }

    public static GameRoom get_room(){
        GameRoom room=null;
        for(int i=0;i<rooms.size();i++){
            GameRoom r = rooms.elementAt(i);
            if(r==null){
                r = new GameRoom();
                r.set_id(i);
                rooms.set(i,r);
                return r;
            }
            if(!r.is_full()){
                room = r;
                break;
            }
        }

        if(room==null){
            room = new GameRoom();
            rooms.add(room);
            room.set_id(rooms.size()-1);
        }

        return room;
    }

    public static void freeRoom(int room_id){
        rooms.set(room_id, null);
    }
}

package com.bankastudio.xando.server;

import com.bankastudio.xando.logic.XandOLogic;

import java.io.IOException;
import java.util.ArrayList;

public class GameRoom {
    int id=-1;
    ArrayList<Client> players;
    XandOLogic xando;

    public GameRoom(){
        players = new ArrayList<Client>();
        xando = new XandOLogic();
    }

    public boolean is_full(){
        return (players.size()>=2);
    }

    public int register_player(Client player){
        int player_id = players.size();
        players.add(player);
        return player_id;
    }

    public void set_id(int i) {
        id=i;
    }

    public void send_start_game() throws IOException {
        for (int i=0; i<players.size();i++) {
            players.get(i).proto.send_start_game(i, get_opponent(i).player_login);
        }
    }

    public void disconnect(int skip_id){
        for (int i=0; i<players.size();i++){
            if(i==skip_id) continue;
            players.get(i).close();
        }
        Server.freeRoom(id);
    }
    public Client get_opponent(int skip_id){
        for (int i=0; i<players.size();i++) {
            if (i == skip_id) continue;
            return  players.get(i);
        }
        return null;
    }

    public void send_mouse_position(int skip_id, int x, int y) throws IOException {
        for (int i=0; i<players.size();i++) {
            if (i == skip_id) continue;
            players.get(i).proto.send_mouse_position(x, y);
        }
    }

    public void make_turn(int player_id, int row, int col) throws IOException {
        if(xando.make_turn(row,col,player_id+1)){
            for (int i=0; i<players.size();i++)
                players.get(i).proto.send_turn_info(player_id+1, row, col);
            if(xando.getWinner()>=0){
                disconnect(-1);
            }
        }
    }

}

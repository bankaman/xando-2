package com.bankastudio.xando.server;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ServerLauncher {

    static void log(String text){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss> ");
        LocalDateTime now = LocalDateTime.now();
        System.out.print(dtf.format(now));
        System.out.println(text);
    }

    static void print(String text){
        System.out.println(text);
    }

    public static void main (String[] arg) {

        int portnum = 3000;
        for(int i=0;i<arg.length;i++){
            if(arg[i].startsWith("-h") || arg[i].startsWith("--h")){
                print("Usage: server.jar [params] ");
                print("\t -p port_number  - set specific port number (default 3000)");
                print("\t -h  - print help and exit");
                return;
            }
            if(arg[i].equals("-p")){
                i++;
                if(i>=arg.length){
                    print("Error! port number expected after -p");
                    return;
                }
                try {
                    portnum = Integer.valueOf(arg[i]);
                }catch (Exception e){
                    print("'"+arg[i]+"' - is not a valid port number!");
                    return;
                }

            }
        }
        print("Use -h to see help.");
        log("Starting server on port #"+Integer.toString(portnum));

        Server server = new Server(portnum);
        server.run();
    }
}
